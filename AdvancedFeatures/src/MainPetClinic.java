package src;

import src.com.sda.*;

import java.awt.print.Pageable;

public class MainPetClinic{


    public static void main(String[] args) {
	// write your code here

        System.out.println("Total Birds are: "+ Birds.getCounter());
        //Duck
        Duck duck = new Duck("Ducky", 12);
        Owner Bob = new Owner("Bob","764767334s","bob@gmail.com");
        duck.setOwner(Bob);
        System.out.println(Bob);
       /* duck.setOwnersName("Bob");
        duck.setOwnersEmail("bob@gmail.com");*/
        System.out.println(duck);
        System.out.println("My name is "+duck.getName() +", my age is "+duck.getAge()
                + " and I can " + duck.canFly(true)+"\n");
        System.out.println("Total Birds are: "+ Birds.getCounter());

        //Chicken
        Chicken chicken = new Chicken("Chicky", 6);
        Owner Alice = new Owner("Alice","367457752","alice@gmail.com");
        chicken.setOwner(Alice);
        System.out.println(Alice);
        /*chicken.setOwnersName("Alice");
        chicken.setOwnersEmail("bob@gmail.com");*/
        System.out.println("My name is "+chicken.name + " and I can " + chicken.canFly(true)+ chicken.canLayEggs(true)
                + chicken.canSwim(false)+"\n");
        System.out.println("Total Birds are: "+ Birds.getCounter());

        Penguin penguin = new Penguin("Pengu", 13);
        Owner Ray = new Owner("Ray","367457752","alice@gmail.com");
        penguin.setOwner(Ray);
        System.out.println(Ray);
        /*penguin.setOwnersName("Ray");
        penguin.setOwnersEmail("ray@gmail.com");*/
        System.out.println("My name is "+penguin.name +", my age is "+penguin.name +", I can "+ penguin.canSwim(true) + penguin.canLayEggs(true)
                + " but "+ penguin.canFly(false) +".\n");
        System.out.println("Total Birds are: "+ Birds.getCounter());

        Birds birds = new Chicken("chicken1", 17);
        birds = penguin;
        birds = duck;
        System.out.println("These are the birds: "+birds+", "+penguin+", "+chicken+". ");
        System.out.println("Total Birds are: "+ Birds.getCounter());

        Birds[] birds1 = new Birds[3];
        birds1[0] = chicken;
        birds1[1] = penguin;
        birds1[2] = duck;
        for (int i=0; i<birds1.length;i++){
            System.out.print("Chicken: ");
            birds1[i].canLayEggs(true);
        }
        for (int i=0; i<birds1.length;i++){
            Birds b = birds1[i];
            //Below here are casting...
            /**if (b instanceof Chicken){
                System.out.print("Chicken: ");
                ((Chicken)b).canFly(true);
            }
            if (b instanceof Duck){
                System.out.print("Duck: ");
                ((Duck)b).canFly(true);
            }
            if (b instanceof Penguin){
                System.out.print("Penguin: ");
                ((Penguin)b).canFly(false);
            } **/
            //Below here is Polymorphism
            b.canFly(true);
        }
        System.out.println("Total Birds are: "+ Birds.getCounter());
        //Polymorphism - Methods can change behaviours inside classes
        //Instead of casting methods, better to use @Override in the sub-class to get to the default method
        //for (int i=0; i<birds1.length;i++){ b.canFly() instead of "((Chicken)b).canFly(true)"
        //

        //Birds b = new Birds("hhh", 7); // This is illegal cos it wants

    }
}
