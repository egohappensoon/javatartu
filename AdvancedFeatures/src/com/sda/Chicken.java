package src.com.sda;

public class Chicken extends Birds {

    public String name;
    public int age;

    public Chicken(String name, int age) {
        super(name, age);
    }

    public String canFly(boolean wings){
//        boolean wings = true;
        if(wings){
            System.out.println("Wooohooo, I can fly!");
        }else{
            System.out.println("Sowweeey, I can't fly!");
        }
        return "I can fly";
    }

    public String canSwim(boolean fins){
        //boolean fins = true;
        if(fins){
            System.out.println("Wooohooo, I can swim!");
        }else{
            System.out.println("Sowweeey, I can't swim!");
        }return "I can swim";
    }

    public String canLayEggs(boolean female){
        //boolean female = true;
        if(female){
            System.out.println("Wooohooo, I can lay eggs!");
        }else{
            System.out.println("Sowweeey, I can't lay eggs!");
        }
        return "I can lay eggs";
    }

    @Override
    public String toString() {
        String s = super.toString();
        return "[Chicken: "+ s + "]";
    }

}
