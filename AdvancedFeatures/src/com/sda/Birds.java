package src.com.sda;

public abstract class Birds {

    private String name;
    private int age;
    static private int counter;
    private  Owner owner;  //calling class Owner to get its properties


    public Birds(String name, int age) {
        this.name = name;
        this.age = age;
        counter++;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static int getCounter() {
        return counter;
    }



    public String canFly(boolean wings){
        //boolean wings = true;
        if(wings){
            System.out.println("Wooohooo, I can fly!");
        }else{
            System.out.println("Sowweeey, I can't fly!");
        }
        return "I can fly";
    }

    public String canSwim(boolean fins){
        if(fins){
            System.out.println("Wooohooo, I can swim!");
        }else{
            System.out.println("Sowweeey, I can't swim!");
        }return "I can swim";
    }

    public String canLayEggs(boolean female){
        if(female){
            System.out.println("Wooohooo, I can lay eggs!");
        }else{
            System.out.println("Sowweeey, I can't lay eggs!");
        }
        return "I can lay eggs";
    }



    @Override
    public String toString() {
        String ownerInfo = "";
        if (owner != null){
            ownerInfo = owner.toString();
        }
        return name + " of " + age + " years old"+ ownerInfo;
        //return name + " of " + age + " years old "+ owner.toString();
    }
}
