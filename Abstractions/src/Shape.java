package src;

public interface Shape {

    double calculateArea();

    double calculateVolume();

    double printRadius();

}
