package src;

public class Application {

    private static Rectangle rectangle;
    //private static Shape shape;

    public static void main(String[] args) {

        //System.out.println(rectangle);

        /*Circle shape = new Circle(2.6);
        double area = shape.calculateArea();*/

        //shape.printRadius();

        //System.out.println("Area is " + rectangle.calculateArea());

        Animal animal = new Cat("Male") {
            @Override
            public void meow() {

            }
        };
        System.out.println(animal.poop());


        /*calculateProduct(new int[]{3});
        findHighestNumber(new int[]{3});
        findHighestNumberSORT(new int[3]);*/


    }

    private static int calculateProduct(int[] numbers){

        int[] highestNumbers = new int[3];
        highestNumbers[0] = findHighestNumber(numbers);
        highestNumbers[1] = findHighestNumber(numbers);
        highestNumbers[2] = findHighestNumber(numbers);
        int result = 1;
        for (int number : highestNumbers) {
            result *= number;
        }
        return result;
    }

    private static int findHighestNumber(int[] numbers){
        int currentHighestNumber = 0;
        int indexOfCurrentHighestNumber = 0;
        //List<Integer>
        for (int index = 0; index < numbers.length; index++) {
            int number = numbers[index];
            if (currentHighestNumber < number) {
                currentHighestNumber = number;
                indexOfCurrentHighestNumber = index;
            }
        }
        numbers[indexOfCurrentHighestNumber] = 0;
        return currentHighestNumber;
    }

    private static int findHighestNumberSORT ( int[] numbers){
        int result = 1;
        for (int i = 0; i < numbers.length; i++) {
            if (i == 3) {
                break;
            }
            result *= numbers[i];
        }
        return result;
    }

}
