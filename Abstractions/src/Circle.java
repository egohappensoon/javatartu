package src;

public abstract class Circle implements Shape {

    private float length;
    private float width;
    private float area;

    public abstract double calculateArea();


}
