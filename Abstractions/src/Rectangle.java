package src;

public abstract class Rectangle implements Shape {

    private float length;
    private float height;


    public void Rectangle(float length, float width) {
        this.length = length;
        this.height = width;
        calculateArea();
        calculateVolume();
    }

    public double calculateArea() {
        //area = L * W
        double area = length * height;
        return area;
    }

    public double calculateVolume() {
        //volume = 2*L + 2*W
        double volume = 2 * (length + height);
        return volume;
    }


}
