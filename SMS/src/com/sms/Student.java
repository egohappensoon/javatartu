package src.com.sms;

import java.lang.instrument.ClassDefinition;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Student {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private LocalDate dob;
    private String email;

    public Student(String firstName, String lastName, String phoneNumber, LocalDate dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.dob = dob;
    }

    //private com.sms.Student[] students;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(int i, String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "com.sms.Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", dob='" + dob + '\'' +
               // ", age='" + LocalDate.now().minus(dob, ChronoUnit.YEARS) + '\'' +
                ", age='" + (LocalDate.now().getYear() - dob.getYear()) +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return true;
    }



}
