package src.com.sms;

import java.time.LocalDate;


public class SMS_App {

    public static void main(String[] args) {


        School unicornUniversity = new School("UnicornUniversiy", "1 Rainbow street");
        //Add students

        Student pinky = new Student("Pinky", "Rose", "283672973", LocalDate.parse("1991-10-10"));
        Student purky = new Student("Purky", "Gesel", "68276882", LocalDate.parse("1995-11-11"));
        Student badStudent = new Student("Bad", "Student", "679768967", LocalDate.parse("1985-01-21"));


        unicornUniversity.addStudent(pinky);
        unicornUniversity.addStudent(purky);
        badStudent.setLastName("Bad student");
        badStudent.setEmail("badStudent@email.com");



        System.out.println(unicornUniversity);

        //Ask users to enter firstName of student

        /*Student foundStudent = unicornUniversity.findStudentWithName("Bad");
        System.out.println(foundStudent);

        unicornUniversity.removeStudent(pinky); //index 0 or pinky
        System.out.println(unicornUniversity);*/


        System.out.println("This is the UPDATE!");
        pinky.setFirstName(0, "pottyyyy"); //index 0 or parameter to change
        System.out.println(pinky);




    }
}
