package src.com.sms;

import java.util.ArrayList;
import java.util.Arrays;

public class School {

    private String name;
    private String address;
    private int colour;
    private int size;
    private String student;

    private Student[] students;
    private int numberOfStudents;  // our student counter

    private ArrayList<Student> studentList ;   // <> Generics


    public School(String name, String address) {
        this.name = name;
        this.address = address;
        students = new Student[100];
        studentList = new ArrayList<>();
        numberOfStudents = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getColour() {
        return colour;
    }

    public void setColour(int colour) {
        this.colour = colour;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }
    public void addStudent(Student student){
        if (numberOfStudents == 100){
            return;
        }
        //students[numberOfStudents] = student;
        //numberOfStudents++;
        studentList.add(student); //add new student
    }
    public void addStudentAtIndex(int index, Student student){
        studentList.add(index, student);
    }

    public Student findStudentWithName(String firstName){
        for (Student student: studentList){
            if (student.getFirstName().equalsIgnoreCase(firstName)){
                return student;
            }
        }
        return null;//throw new Exception("No student with this name exmist");
    }

    public void removeStudent(Student index){
        studentList.remove(index);
    }

   /* Not usefull...just leaving it here!
   public void updateStudent(int index, Student student){
        studentList.set(index, student);
    }*/

    @Override
    public String toString() {
        return "School{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", colour=" + colour +
                ", size=" + size +
                ", student='" + student + '\'' +
                ", students=" + Arrays.toString(students) +
                ", numberOfStudents=" + numberOfStudents +
                ", studentList=" + studentList +
                '}';
    }
}
