package src.main.java;

public class Animal {

    String breathe;
    String eat;
    String type;

    public Animal(String typeA, String breatheA, String eatA){
        type = typeA;
        eat = eatA;
        breathe = breatheA;
        System.out.println("Constructor");
    }

    public  void setBreathe(){
        breathe = "Inhale";
        System.out.println(breathe);
    }
    public  void setEat(){
        eat = "Yummy";
        System.out.println(eat);
    }
    public  void printType(){
        System.out.println(type);
    }

    @Override
    public String toString() {
        return type;
    }
}
