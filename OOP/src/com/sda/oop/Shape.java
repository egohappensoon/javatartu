package src.com.sda.oop;

public class Shape {

    private float length;
    private float width;
    private String colour;
    private float area;
    private float volume;


    public float getLength() {
        System.out.println("This is adding extras: ");
        return length;// = length + 2.5f;

    }

    public void setLength(float length) {
        System.out.println("Setter length from Parent: ");
        //this.length = length * 2;
        this.length = length;
        areaOfShape();
        volumeOfShape();
    }

    public float getWidth() {
        return width = width + 1;
    }

    public void setWidth(float width) {
        //System.out.println("Setter width from Parent: ");
        this.width = width;
    }

    public String getColour() {
        return colour = colour + "s";
    }

    public void setColour(String colour) {
        //System.out.println("Setter colour from Parent: ");
        this.colour = colour;
    }

    public float getArea() {
        return area = area + 5;
    }

    public void setArea(float area) {
       // System.out.println("Setter area from Parent: ");
        this.area = area + 1;
    }

    public float getVolume() {
        return volume = volume + 5;
    }

    public void setVolume(float volume) {
        //System.out.println("Setter volume from Parent: ");
        this.volume = volume + 1;
    }

    @Override
    public String toString() {
        return "com.sda.oop.Shape{" +
                "length=" + length +
                ", width=" + width +
                ", colour='" + colour + '\'' +
                ", area=" + area +
                ", volume=" + volume +
                '}';
    }

    //Calulate the area
    float areaOfShape() {
        area = length * width;
        return area;
    }
    float volumeOfShape() {
        volume = length * length * length;
        return volume;
    }
}
