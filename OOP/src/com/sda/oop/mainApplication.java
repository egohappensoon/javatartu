package src.com.sda.oop;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class mainApplication {

    public static void main(String[] args) {

       /* Shape shape = new Shape();
        System.out.println(shape);

        shape.setLength(2);
        System.out.println(shape);

        shape.setLength(4);
        System.out.println(shape);
        //System.out.println(shape.getLength());
        //shape.setColour("White");
       // shape.getColour("Black");
        //shape.setArea(13);
       // shape.getArea(16);
        //shape.setVolume(12);
        //shape.getVolume(11);
        //shape.setWidth(15);
       // shape.getWidth(17);

       // System.out.println(shape.toString());*/


        //Rectangle
        /*Rectangle rectangle = new Rectangle();
        rectangle.setLength(1200);
        rectangle.setArea(1200);*/

        //System.out.println(rectangle.getArea());

        //Area = length*width;

        //System.out.println(shape.areaOfShape());
        //System.out.println(shape.volumeOfShape());


        /*Circle circle = new Circle();
        circle.setRadius(3);
        System.out.println(circle); */

        /*Rectangle rectangle = null;
        try {
            rectangle = new Rectangle(3,5);
            //rectangle.paintRectangle();
            //rectangle.paintEdges();
            rectangle.paintEdgez();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }*/
        //System.out.println(rectangle);

        /** PRINT THE SUM OF EVEN NUMBERS**/

        int evenNumbers = 10;
        int sumEven = 0;

        System.out.println("Printing the sum of Even numbers between 1 and " + evenNumbers);

        for (int i = 0; i <= evenNumbers; i++) {

            // if the number is divisible by 2 then it is even
            if (i % 2 == 0) {
                sumEven += i;
                //System.out.print(i + " ");
            }
        }
        System.out.println(sumEven);

        /** PRINT THE SUM OF EVEN NUMBERS**/

        int oddNumbers = 10;
        int sumOdd = 0;

        System.out.println("Printing the sum of Odd numbers between 1 and " + oddNumbers);

        for (int i = 0; i <= evenNumbers; i++) {

            // if the number is divisible by 2 then it is even
            if (i % 2 == 1) {
                sumOdd += i;
                //System.out.print(i + " ");
            }
        }
        System.out.println(sumOdd);



        /*int n, sumE = 0, sumO = 0;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter the number of elements in array:");
        n = s.nextInt();
        int[] a = new int[n];
        System.out.println("Enter the elements of the array:");*/
        int n = 10, sumE = 0, sumO = 0;
        /*for(int i = 0; i < n; i++) {
            i = s.nextInt();
        }*/
        for (int i = 0; i <= n; i++) {
            if (i % 2 == 0) {
                //sumE = sumE + i;
                sumE += i;
            } else if (i % 2 == 1) {
                //sumO = sumO + i;
                sumO += i;
            }
        }
        System.out.println("Sum of Even Numbers:" + sumE);
        System.out.println("Sum of Odd Numbers:" + sumO);

        //FizzBuzz();
        //FizzBuzz2();
        //iLikeBeer();
        //iLikeBeerUSER();
       // System.out.println(isPalindrome("madam"));
        System.out.println("1st non-repeating number/letter is: " + nonRepeatingValue(new String[]{"4","4", "4","1","3","5","6","8"}));




    }

    //FIZZBUZZ
    private static void FizzBuzz() {
        int f = 20;
        for (int a = 1; a < f; a++) {
            if (a % 3 == 0 && a % 5 == 0) {
                System.out.println("FizzBuzz");
            }
            else if (a % 3 == 0) {
                //sumE = sumE + i;
                System.out.println("Fizz");
            } else if (a % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(a);
            }
        }

    }

    private static void FizzBuzz2() {

        Scanner s = new Scanner(System.in);
        System.out.print("Enter the number of elements in array:");
        int f = s.nextInt();
        if (f <0){   ////for negative numbers conversion
            f = -f;
        }
        //for (int a = 1; a < Maths.ab(f); a++) { //for negative numbers conversion
        for (int a = 1; a < f; a++) {
            if (a % 3 == 0 && a % 5 == 0) {
                System.out.println("FizzBuzz");
            }
            else if (a % 3 == 0) {
                System.out.println("Fizz");
            }else if (a % 4 == 0) {
                System.out.println("Fuzzy");
            } else if (a % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(a);
            }
        }
    }

    public static void iLikeBeer(){
        //Locating Vowels in a sentence
        String iLikeBeer = "Mulle meeldib õlu";
        System.out.println(iLikeBeer);
        iLikeBeer = iLikeBeer.replace(" ", "");
        List<String> vowels = new ArrayList<>();
        vowels.add("a");
        vowels.add("e");
        vowels.add("i");
        vowels.add("o");
        vowels.add("u");
        vowels.add("ä");
        vowels.add("õ");
        vowels.add("ü");
        vowels.add("ö");
        int numberOfVowels = 0;
        for (String vowel : iLikeBeer.split("")){
            if (vowels.contains(vowel)){
                vowels.remove(vowel);   //Remove a vowel that is double or more
                numberOfVowels++;
            }
        }
        System.out.println(numberOfVowels);
    }

    public static void iLikeBeerUSER(){
        //Locating Vowels in a sentence from user
        Scanner s = new Scanner(System.in);
        System.out.print("Please, enter a sentence in Estonian on English: ");
        String iLikeBeer = s.nextLine();
        //System.out.println(iLikeBeer);
        iLikeBeer = iLikeBeer.replace(" ", "");
        List<String> vowels = new ArrayList<>();
        vowels.add("a");
        vowels.add("e");
        vowels.add("i");
        vowels.add("o");
        vowels.add("u");
        vowels.add("ä");
        vowels.add("õ");
        vowels.add("ü");
        vowels.add("ö");
        int numberOfVowels = 0;
        for (String vowel : iLikeBeer.split("")){
            //if (!vowels.contains(vowel)){  // Doesn't contain vowels
            if (vowels.contains(vowel)){
                //vowels.remove(vowel);   //Remove a vowel that is double or more
                numberOfVowels++;
            }
        }
        System.out.println(numberOfVowels);
    }


    public static boolean isPalindrome (String palis){
        String palindrome = "";
        String[] palisChars = palis.split("");
        for (int i = palisChars.length - 1; i>= 0; i--){
            palindrome += palisChars[i];
        }
        return palindrome.equals(palis);
    }

    private static String nonRepeatingValue(String[] valuesString){
        List<String> values = new ArrayList<>();
        for (String letter: valuesString){
            if (!values.contains(letter)){
                values.add(letter);
            }else{
                values.remove(letter);
            }
        }return values.get(0);
    }

    /*
     * Task 1
     * Given a list of integers, find a pair that gives maximum product
     * (1,2,3,4) -> 12
     */


    /*
     * Task 2
     * Find the greatest common divisor of an input list
     * (2,4) -> 2
     */


    /*
     * Task 3
     * Create a heart rate data type (class) and given a list of heart rates,
     * find the average per minute
     */




}
