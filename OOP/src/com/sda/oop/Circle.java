package src.com.sda.oop;

public class Circle {
    private  float radius;
    private  float diameter;
    private  float circumfrence;
    private  float area;



    public void setRadius(float inputRadius) {
        radius = inputRadius * 2;
        diameter = radius * 2;
        calculateCircumfrence();
        calculateArea();
    }

    private void calculateCircumfrence() {
        //2 * Math.PI * radius
        circumfrence = (float) (2 * Math.PI * radius);
    }

    private void calculateArea() {
        //2 * Math.PI * radius
        area = (float) (Math.PI * Math.pow(radius,2));
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", diameter=" + diameter +
                ", circumfrence=" + circumfrence +
                ", area=" + area +
                '}';
    }
}
