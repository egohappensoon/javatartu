package hm1.car;

public class Window {

    private float tintedPercentage;
    private String colour = "black";
    private Shape  shape;

    public Window(Shape shape) {
        this.tintedPercentage = tintedPercentage;
        this.colour = colour;
        this.shape = shape;
    }

    public float isTintedPercentage() {
        return tintedPercentage;
    }

    public void setTintedPercentage(float tintedPercentage) {
        this.tintedPercentage = tintedPercentage;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return "Window{" +
                "tintedPercentage=" + tintedPercentage +
                ", colour='" + colour + '\'' +
                ", shape=" + shape +
                '}';
    }
}
