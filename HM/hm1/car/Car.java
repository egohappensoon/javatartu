package hm1.car;


import java.util.Arrays;

public class Car {

    private  String colour;
    private  String brand;
    private  String manufacturer;
    private  String model;
    private  int year;


    private String seatColour;
    private Wheel[] wheel;
    //private Window window;
    private Engine engine;
    private Door[] door;


    public Car(String colour, String brand, String manufacturer, String model, int year,
               String seatColour, Wheel[] wheel, Engine engine, Door[] door) throws Exception {

        if (wheel.length!= 4){
            throw new Exception("That's not a car!");
        }
        if (door.length < 2){
            throw new Exception("That's not a car!");
        }
        this.colour = colour;
        this.brand = brand;
        this.manufacturer = manufacturer;
        this.model = model;
        this.year = year;
        this.seatColour = seatColour;
        this.wheel = wheel;
        this.engine = engine;
        this.door = door;
    }


    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getSeatColour() {
        return seatColour;
    }

    public void setSeatColour(String seatColour) {
        this.seatColour = seatColour;
    }

    public Wheel[] getWheel() {
        return wheel;
    }

    public void setWheel(Wheel[] wheel) {
        this.wheel = wheel;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Door[] getDoor() {
        return door;
    }

    public void setDoor(Door[] door) {
        this.door = door;
    }

    @Override
    public String toString() {
        return "Car{" +
                "colour='" + colour + '\'' +
                ", brand='" + brand + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", seatColour='" + seatColour + '\'' +
                ", wheel=" + Arrays.toString(wheel) +
                ", engine=" + engine +
                ", door=" + Arrays.toString(door) +
                '}';
    }


   /* Car car = new Car("black","Kia", "Kia", "Sorento",
            2020, "Blue",wheel[4], "v12", "four");
*/




}
