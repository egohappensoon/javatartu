package hm1.car.HeartRate;

import java.time.LocalDateTime;

public class HearRate {
    private float value;
    private LocalDateTime time;

    public HearRate(float value, LocalDateTime time) {
        this.value = value;
        this.time = time;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "HearRate{" +
                "value=" + value +
                ", time=" + time +
                '}';
    }
}
