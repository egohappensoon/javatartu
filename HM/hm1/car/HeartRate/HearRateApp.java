package hm1.car.HeartRate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HearRateApp {
    public static void main(String[] args) {

        List<HearRate> hearRates = new ArrayList<>();

        Random random = new Random();

        LocalDateTime now = LocalDateTime.now();
        for (int i = 0; i < 100; i++) {
            int value = random.nextInt(200);
            now = now.plusSeconds(1);
            hearRates.add(new HearRate(value, now));
        }
        float sum = 0;
        for(HearRate hearRate: hearRates){
            sum += hearRate.getValue();
        }
        System.out.println(hearRates);
        System.out.println(sum/hearRates.size());

    }
}
