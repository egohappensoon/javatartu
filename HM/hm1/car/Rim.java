package hm1.car;

public class Rim {

    private String colour = "black";
    private float radius;
    private float width;
    private boolean rimType;

    public Rim(String colour, float radius, float width, String rimType) {
        this.colour = colour;
        this.radius = radius;
        this.width = width;
        this.rimType = rimType;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public boolean isRimType() {
        return rimType;
    }

    public void setRimType(boolean rimType) {
        this.rimType = rimType;
    }

    @Override
    public String toString() {
        return "Rim{" +
                "colour='" + colour + '\'' +
                ", radius=" + radius +
                ", width=" + width +
                ", rimType=" + rimType +
                '}';
    }


}
