package hm1.car;

public class Engine {

    private String fuelType;
    private float power;
    private float volume;

    public Engine(String fuelType, float power, float volume) {
        this.fuelType = fuelType;
        this.power = power;
        this.volume = volume;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public float getPower() {
        return power;
    }

    public void setPower(float power) {
        this.power = power;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "fuelType='" + fuelType + '\'' +
                ", power=" + power +
                ", volume=" + volume +
                '}';
    }


}
