package hm1.car;

public class Wheel {
    private float radius;
    private float width;
    private String colour;

    public Wheel(float radius, float width, String colour, Rim rim) {
        this.radius = radius;
        this.width = width;
        this.colour = colour;
        this.rim = rim;
    }

    private Rim rim;

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Rim getRim() {
        return rim;
    }

    public void setRim(Rim rim) {
        this.rim = rim;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "radius=" + radius +
                ", width=" + width +
                ", colour='" + colour + '\'' +
                ", rim=" + rim +
                '}';
    }


}
