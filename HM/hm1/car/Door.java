package hm1.car;

public class Door {

    private Window window;
    private String colour;
    private String openingType;
    private Shape shape;

    public Door(Window window, String colour, String openingType, Shape shape) {
        this.window = window;
        this.colour = colour;
        this.openingType = openingType;
        this.shape = shape;
    }

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getOpeningType() {
        return openingType;
    }

    public void setOpeningType(String openingType) {
        this.openingType = openingType;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return "Door{" +
                "window=" + window +
                ", colour='" + colour + '\'' +
                ", openingType='" + openingType + '\'' +
                ", shape=" + shape +
                '}';
    }
}
