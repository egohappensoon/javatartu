package hm1.car;

import hm1.car.*;

public class CarApp {

    public static void main(String[] args) {

        Rim rim = new Rim("Silver", 20, 5, "Alloy");

        Wheel[] wheels = new Wheel[4];
        Door[] doors = new Door[4];

        for (int i= 0; i< wheels.length; i++){
            Wheel wheel = new Wheel(20, 5,"Blue", rim);
            wheels[i] = wheel;
        }
        for (Wheel wheel: wheels) {
           // wheel = new Wheel(20, 5, rim);
            wheel.getRim().setColour("Yellow");
        }
        //System.out.println(rim);

        for (Door door: doors) {
            Shape shape = new Shape(12, 20);
            Window window = new Window(shape);
            window.setColour("Orange");
            window.setTintedPercentage(0.685F);

            Shape windowShape = new Shape(10, 12);
            door = new Door(window,"Black","Side",windowShape);
            shape.setHeight(19);
            door.getWindow().getShape().setHeight(20);
        }

        Engine engine = new Engine("Electric", 20, 5000);
        try {
            Car tesla = new Car("Navyblue", "Tesla","Tesla","Model Y", 2020,"Beige", wheels, engine,doors);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

