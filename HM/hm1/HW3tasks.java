package hm1;

import hm1.car.Car;
import hm1.car.Wheel;
import org.w3c.dom.ls.LSOutput;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;




public class HW3tasks {

    // The MAIN class that runs the program
    public static void main(String[] args) throws ParseException {

        int[] arr = {1, 4, 3, 6, 7, 0, 8, 9};
        int n = arr.length;
        maxProduct(arr, n);

         int a = 98, b = 56;
        System.out.println("The greatest common divisor of " + a +" and " + b + " is " + greatestCommonDivisor(a, b));

        greatestCommonDivisorUSER();
        System.out.println();

        GCD();
        System.out.println();

        averagePerMinute();
        System.out.println();

        averagePerMinuteWHILE();
        System.out.println();

        averagePerMinuteUSER();
        System.out.println();




    }


    /* Task 1
     * Given a list of integers, find a pair that gives maximum product
     * (1,2,3,4) -> 12
     */
    // int arr[0..n-1]
    private static void maxProduct(int[] arr, int n) {
        /*if (n < 2) {
            System.out.println("No pairs found!");
            return;
        }*/

        // Initialize maximum product pair
        int a = arr[0], b = arr[1];

        // Loop through every possible pair and keep track of max product
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                if (arr[i] * arr[j] > a * b) {
                    a = arr[i];
                    b = arr[j];
                }

        System.out.println();
        System.out.println("The maximum integers pair are " + a + " and " + b );
        System.out.println("Maximum product of pair is " + a * b );
        System.out.println();

    }



    /*
     * Task 2
     * Find the greatest common divisor of an input list
     * (2,4) -> 2
     */
    private static int greatestCommonDivisor(int a, int b) {

        if (a == 0 || b == 0)  // Everything divides 0
            return 0;
        if (a == b) // base case
            return a;
        if (a > b)  // a is greater
            return greatestCommonDivisor(a-b, b);
        return greatestCommonDivisor(a, b-a);
    }

    //With input from Users
    private static void greatestCommonDivisorUSER() {
        int num1, num2, greatestCommonDivisorUSER=1;

        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.print("Enter first number: ");
        num1 = (int)scanner.nextInt();

        System.out.print("Enter second number: ");
        num2 = (int)scanner.nextInt();

        for (int i = 1; i <= num1 && i <= num2; i++) {
            if (num1 % i == 0 && num2 % i == 0)
                greatestCommonDivisorUSER = i;
        }

        System.out.printf("The greatest common divisor of %d and %d is:  %d", num1, num2, greatestCommonDivisorUSER);
        System.out.println();
    }


    private static void GCD() {
        int num1 = 55, num2 = 121;

        while (num1 != num2) {
            if(num1 > num2)
                num1 = num1 - num2;
            else
                num2 = num2 - num1;
        }
        System.out.printf("The GCD of given numbers is: %d", num2);
    }

    private static int GCDinClass(List<Integer> numbers) {
        if (numbers.contains(1)){
            return 1;
        }


        System.out.printf("The GCDinClass of given numbers is: %d");
        return 0;
    }


    /*
     * Task 3
     * Create a heart rate data type (class) and given a list of heart rates,
     * find the average per minute
     */

    private static void averagePerMinute() throws ParseException {
        String timeInHHmmss = "08:45:50, 06:30:10, 09:04:40, 05:06:35, 01:06:07, 05:09:08, 06:06:37, 21:16:27, 02:39:48";
        String[] split = timeInHHmmss.split(", ");
        long sum = 0L;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        for (int i = 0; i < split.length; i++) {
            sum += sdf.parse(split[i]).getTime();
        }
        Date avgDate = new Date((sum / split.length));
        System.out.println("The average per minute is: " + sdf.format(avgDate));

    }
    private static void averagePerMinuteWHILE() throws ParseException {
        String timeInHHmmss = "08:45:50, 06:30:10, 09:04:40, 05:06:35, 01:06:07, 05:09:08, 06:06:37, 02:39:48";
        String[] split = timeInHHmmss.split(", ");
        long sum = 0L;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        int i = 0;
        while (i < split.length) {
            sum += sdf.parse(split[i]).getTime();
            i++;
        }
        Date avgDate = new Date((sum / split.length));
        System.out.println("WHILE! The average per minute is: " + sdf.format(avgDate));

    }

    private static void averagePerMinuteUSER() throws ParseException {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.print("Please enter heart rates separated by commas in this format('09:04:40, 05:06:35'): ");
        String timeInHHmmss = scanner.nextLine();
        String[] split = timeInHHmmss.split(", ");
        long sum = 0L;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        for (String s : split) {
            sum += sdf.parse(s).getTime();
        }
        Date avgDate = new Date((sum / split.length));
        System.out.println("User's input average per minute is: " + sdf.format(avgDate));

    }



}
